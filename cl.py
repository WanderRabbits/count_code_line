#!/usr/bin/python3
import os

def count_dir(dirname):
	count = 0
	filelist = os.listdir(dirname)
	for filename in filelist:
		if filename == 'venv' or filename == 'cmake-build-debug' or filename == 'node_modules' or filename == 'public' or filename == '.mvn' or filename == 'cf-tool' or filename == 'out' or filename == '.idea' or filename == 'mod':
				continue
		filename = dirname + '\\' + filename
		if os.path.isdir(filename):
			count += count_dir(filename)
		elif filename[-3:] == '.cc' or filename[-3:] == '.py' or filename[-3:] == '.go' or filename[-3:] == '.rs' or filename[-4:] == '.cpp' or filename[-5:] == '.java' or filename[-2:] == '.c':
			count += len(open(filename, 'r', encoding='utf-8', errors='ignore').readlines())
	return count

print("目前写了{}行代码" % (count_dir(os.getcwd())))
